package com.example.memo.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.memo.Models.DataModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "memos_database";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_memos = "memos";
    private static final String KEY_ID = "id";
    private static final String TITLE = "title";
    private static final String TEXT = "text";



    /*CREATE TABLE pays ( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, phone_number TEXT......);*/

//    private static final String CREATE_TABLE_memos = "CREATE TABLE "
//            + TABLE_memos + "(" + KEY_ID
//            + " INTEGER PRIMARY KEY AUTOINCREMENT," + TITLE +  TEXT + " TEXT );";

    private static final String CREATE_TABLE_memos = "CREATE TABLE " + TABLE_memos + " ("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TITLE + " TEXT NOT NULL ,"
            + TEXT + " TEXT NOT NULL "
            + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        Log.d("table", CREATE_TABLE_memos);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_memos);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_memos + "'");
        onCreate(db);
    }

    public long addMemos(DataModel dataModel) {
        String title = dataModel.getTitle();
        String text = dataModel.getText();
        SQLiteDatabase db = this.getWritableDatabase();
        // Creating content values
        ContentValues values = new ContentValues();
        values.put(TITLE, title);
        values.put(TEXT, text);

        // insert row in memos table
        long insert = db.insert(TABLE_memos, null, values);

        return insert;
    }

    public List<DataModel> getMemos() {
        String title="";
        String text="";

        String[] projection = {KEY_ID, TITLE, TEXT};
        // tri :
        String tri = KEY_ID + " DESC " ;
        // accès en lecture (query) :
        SQLiteDatabase db = this.getReadableDatabase();
        // requête :
        Cursor cursor = db.query(
                TABLE_memos,  // table sur laquelle faire la requète
                projection,                         // colonnes à retourner
                null,                    // colonnes pour la clause WHERE (inactif)
                null,                 // valeurs pour la clause WHERE (inactif)
                null,                     // GROUP BY (inactif)
                null,                      // HAVING (inactif)
                 tri,                             // ordre de tri
                null);                      // LIMIT (inactif)

        List<DataModel> listeMemos = new ArrayList<>();
        if (cursor != null)
        {
            try
            {
                cursor.moveToFirst();
                while (!cursor.isAfterLast())
                {
                    // conversion des données remontées en un objet métier :
                    listeMemos.add(new DataModel(cursor.getInt(cursor.getColumnIndex(KEY_ID)), cursor.getString(cursor.getColumnIndex(TITLE)), cursor.getString(cursor.getColumnIndex(TEXT))));
                    cursor.moveToNext();
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
            finally
            {
                cursor.close();
            }
        }

        return listeMemos;
    }

}
