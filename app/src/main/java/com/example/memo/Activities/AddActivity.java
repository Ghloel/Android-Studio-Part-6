package com.example.memo.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.memo.Adapters.RecyclerViewAdapter;
import com.example.memo.Interfaces.RecyclerViewClickListener;
import com.example.memo.Listeners.RecyclerTouchListener;
import com.example.memo.Models.DataModel;
import com.example.memo.R;

import java.util.ArrayList;
import java.util.List;

public class AddActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<DataModel> dataModelList;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        //initializing view objects
        //The view objects


        Button buttonSubmit = findViewById(R.id.buttonSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                dataModelList = new ArrayList<>();
//                dataModelList.add(new DataModel(9, "test", "test"));
//                RecyclerViewAdapter adapter = new RecyclerViewAdapter(dataModelList);
//                recyclerView.setAdapter(adapter);
                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    int count = extras.getInt("count");
                }

                EditText editTitle = findViewById(R.id.editTitle);
                EditText editText = findViewById(R.id.editText);

                final String title = editTitle.getText().toString();
                final String text = editText.getText().toString();

                System.out.println(title);
                System.out.println(text);

                Intent intent = new Intent(AddActivity.this, MainActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("text", text);
//                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
//                setResult(2, intent);
                finish();

//                AddActivity.this.startActivity(intent);

            }
        });



    }
}
