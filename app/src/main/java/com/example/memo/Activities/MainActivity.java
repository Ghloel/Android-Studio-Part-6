package com.example.memo.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.memo.Adapters.RecyclerViewAdapter;
import com.example.memo.Database.DatabaseHelper;
import com.example.memo.Interfaces.RecyclerViewClickListener;
import com.example.memo.Listeners.RecyclerTouchListener;
import com.example.memo.Models.DataModel;
import com.example.memo.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<DataModel> dataModelList;
    private RecyclerView recyclerView;

    public int CountMemo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String title = "";
        String text = "";
        CountMemo = (int) getDatas();

        // Instantiate the recyclerView
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setFocusable(false);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            title = extras.getString("title");
            text = extras.getString("text");
            Toast.makeText(this, "Nouveau mémo : " + title, Toast.LENGTH_LONG).show();
            setData(title, text);
        }

        setDataToRecyclerView();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {

                String title = dataModelList.get(position).getTitle();
                String text = dataModelList.get(position).getText();
                int id = dataModelList.get(position).getId();
                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("text", text);
                intent.putExtra("id", id);
                MainActivity.this.startActivity(intent);
            }
        }));


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                intent.putExtra("count" , CountMemo);
//                startActivityForResult(intent, 2);
                MainActivity.this.startActivity(intent);

                finish();
            }
        });

    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {
            String title = data.getStringExtra("title");
            String text = data.getStringExtra("text");
            Toast.makeText(this, "Nouveau mémo : " + title, Toast.LENGTH_LONG).show();
            this.setData(title, text);
        }
    }*/

    private long getDatas(){
        dataModelList = new ArrayList<>();
        int index = 1;
        long lastResult = CountMemo;
        DatabaseHelper db = new DatabaseHelper(this);
        List<DataModel> dbMemo = db.getMemos();
        if (dbMemo.size() == 0){
            for (int i = 0; i <= 8 ; i++ ){
//            dataModelList.add(new DataModel( i-1, "Memo "+i, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est."));
                DataModel newModel = new DataModel( i, "Memo "+i, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est.");
                long result = db.addMemos(newModel);
                System.out.println(result);
                if (result != 0){
                    dataModelList.add(newModel);
                }
                lastResult = result;
                index++;
            }
        } else {
            lastResult = dbMemo.size();
            dataModelList = dbMemo;
        }
        return lastResult;
    }

    private void setData(String title, String text) {

        if (!title.isEmpty() && !text.isEmpty()){
            DatabaseHelper db = new DatabaseHelper(this);
            long result = db.addMemos(new DataModel( CountMemo+1, title, text));
            System.out.println(result);
            if (result != 0){
                dataModelList = db.getMemos();
            }
//            dataModelList.add(new DataModel(CountMemo+1,title,text));
        }
    }

    private void setDataToRecyclerView() {
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(dataModelList);
        recyclerView.setAdapter(adapter);
    }
}
