package com.example.memo.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.memo.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ResultActivity extends AppCompatActivity {

    private static final String URL = "https://httpbin.org/post";
    private static final String KEY_FORM = "form";
    private static final String KEY_MEMO = "memo";
    private static final String KEY_POSITION = "position";
    public String title;
    public String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        TextView tvID = findViewById(R.id.tvID);
        TextView tvTitle = findViewById(R.id.tvTitle);
        TextView tvText = findViewById(R.id.tvText);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int id = extras.getInt("id");
            title = extras.getString("title");
            text = extras.getString("text");

            tvID.setText(String.valueOf(id));
            tvTitle.setText(title);
            tvText.setText(text);
        }



    }

    public void onClick(View view) {
        // client HTTP :
        AsyncHttpClient client = new AsyncHttpClient();

        // paramètres :
        RequestParams requestParams = new RequestParams();
        requestParams.put("memo", title);

        // appel :
        client.post(URL, requestParams, new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String retour = new String(responseBody);
                try
                {
                    JSONObject jsonObject = new JSONObject(retour);
                    JSONObject jsonObjectForm = jsonObject.getJSONObject(KEY_FORM);
                    Toast.makeText(ResultActivity.this, jsonObjectForm.getString(KEY_MEMO), Toast.LENGTH_LONG).show();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                System.out.println(error);
            }
        });
    }
}
